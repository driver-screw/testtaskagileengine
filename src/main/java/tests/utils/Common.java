package tests.utils;

import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import tests.BaseTest;

import java.util.Arrays;
import java.util.Random;

import static io.restassured.RestAssured.given;

public class Common extends BaseTest {
    private static String MESSAGE = "message";


    static public JsonPath rawToJson(Response response) {
        JsonPath jsonPath = new JsonPath(response.asString());
        return jsonPath;
    }

    public static Response getNodeById(String nodeId, String userAccessToken) {
        Response response = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
                .when()
                .param("access_token", userAccessToken)
                .get(nodeId)
                .then()
                .contentType(ContentType.JSON)
                .assertThat().statusCode(200)
                .extract().response();

        return response;
    }

    public static boolean deleteNodeById(String nodeId, String pageAccessToken) {
        Response response = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
                .when()
                .param("access_token", pageAccessToken)
                .delete(nodeId)
                .then()
                .contentType(ContentType.JSON)
                .assertThat().statusCode(200)
                .extract().response();

        JsonPath jsonPathAddPostResponse = Common.rawToJson(response);
        boolean result = jsonPathAddPostResponse.get("success");
        return result;
    }

    public static String createNewTextPost(String message, String pageAccessToken) {
        Response createPostResponse = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
                .when()
                .param(MESSAGE, message)
                .param("access_token", prop.getProperty("PAGE_ACCESS_TOKEN"))
                .post(Endpoints.getCreatePostEndpoint(prop.getProperty("PAGE_ID")))
                .then()
                .assertThat().statusCode(200)
                .extract().response();

        JsonPath jsonPathAddPostResponse = Common.rawToJson(createPostResponse);
        String nodeId = jsonPathAddPostResponse.get("id");
        return nodeId;
    }

    public static Response tryCreateNewTextPost(String message, String pageAccessToken, int expectedSatusCode) {
        Response createPostResponse = given().filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()))
                .when()
                .param(MESSAGE, message)
                .param("access_token", pageAccessToken)
                .post(Endpoints.getCreatePostEndpoint(prop.getProperty("PAGE_ID")))
                .then()
                .assertThat().statusCode(expectedSatusCode)
                .extract().response();

        return createPostResponse;
    }

    public static String getRandomSuffix() {
        Random random = new Random();
        int first = random.nextInt(26) + 65;
        char firstChar = (char) first;
        int suffix = 10000 + random.nextInt(89999);
        return Character.toString(firstChar) + suffix;
    }

    public String getUserAccessToken(){
        return "";
    }

    public String getPageAccessToken(){
        return "";
    }
}
