package tests;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import tests.utils.Common;


public class CreatePost extends BaseTest {

    final String MESSAGE = "message";
    private String nodeId = "";

    @After
    public void deleteNode() {
        if(!"".equals(nodeId)) {
            Common.deleteNodeById(nodeId, prop.getProperty("PAGE_ACCESS_TOKEN"));
            nodeId = "";
        }
    }

    @Test
    public void createTextPostSuccess() {
        String message = "Created post message content." + Common.getRandomSuffix();
        nodeId = Common.createNewTextPost(message, prop.getProperty("PAGE_ACCESS_TOKEN"));
        assertCreatedPost(nodeId, message);
    }

    @Test
    public void createTextPostInvalidTokenFail() {
        String message = "Created post message content." + Common.getRandomSuffix();
        Common.tryCreateNewTextPost(message, Common.getRandomSuffix(), 400);
    }


    private void assertCreatedPost(String postId, String expectedMessage) {
        Response getPostResponse = Common.getNodeById(postId, prop.getProperty("USER_ACCESS_TOKEN"));
        JsonPath jsonPathGetPostResponse = Common.rawToJson(getPostResponse);
        String actualMessage = jsonPathGetPostResponse.get(MESSAGE);
        Assert.assertTrue(expectedMessage.equals(actualMessage));
    }

}
