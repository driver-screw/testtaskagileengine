package tests;

import io.restassured.RestAssured;
import org.junit.Before;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class BaseTest {

    protected static Properties prop;

    @Before
    public void getProperties() throws IOException {
        prop = new Properties();
        FileInputStream fis = new FileInputStream("src/main/resources/env.properties");
        prop.load(fis);
        RestAssured.baseURI = prop.getProperty("HOST");
    }
}
